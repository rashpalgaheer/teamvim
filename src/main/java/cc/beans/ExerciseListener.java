/**
 * 
 */
package cc.beans;

import org.apache.log4j.BasicConfigurator;

/**
 * @author Stefanski
 *
 */

import org.springframework.context.support.GenericXmlApplicationContext;

public class ExerciseListener {
	public static void main(String[] args) {
		//BasicConfigurator.configure();
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:spring-beans.xml");
		ctx.refresh();

		while (true) {
		}
	}
}