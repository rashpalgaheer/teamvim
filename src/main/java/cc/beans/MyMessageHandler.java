package cc.beans;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.QueueReceiver;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.MessagePostProcessor;
public class MyMessageHandler {

	
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	public MyMessageHandler() {
		context = new ClassPathXmlApplicationContext("spring-beans.xml");
		destination = context.getBean("destination", Destination.class);
		replyTo = context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}


	public static void main(String[] args) {
		MyMessageHandler mmh = new MyMessageHandler();
		mmh.run();
	}

	private void run() {
		makeMessage("bvas");
		System.out.println("Message Sent to JMS Queue:- ");

	}
	
	public void makeMessage(String order) {
		jmsTemplate.send(destination, new MessageCreator() {
			
			
			
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage();
				message.setText(order);
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("123-10001");
				System.out.println( message.getJMSCorrelationID());
				System.out.println(message.getJMSReplyTo());
			//	QueueReceiver rece = ;
			//	System.out.println("hn " + rece.getText());
			
				return message;
			}
		});
	
	//	Message m = context.createConsumer(replyTo).receive();
	//	System.out.println("hn " + m);
	}
	
}
