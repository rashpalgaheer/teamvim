/**
 * 
 */
package cc.beans;
/**
 * @author Stefanski
 *
 */
import java.util.Collection;
import java.util.Enumeration;


// for JMS Messaging
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.Model.Trader;

public class MyMessageListener implements MessageListener {

	@SuppressWarnings("unchecked")
	public void onMessage(Message m) {
		System.out.println( "In Listener");
		TextMessage message = (TextMessage) m;
	//	MessageConsumer consumer;
		//consumer = session.createConsumer();
		
		try {
//			Enumeration<String> myEnum = (Enumeration<String>) message.getMapNames();
//			
//			while (myEnum.hasMoreElements()){
//			       System.out.println("reply: " + myEnum.nextElement());
//			}
			//message = consumer.receive();
			 System.out.println("reply: " + message.getText());
			 Trader.orderListener(message.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
