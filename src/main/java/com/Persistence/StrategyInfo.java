package com.Persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name ="Strategy")
public class StrategyInfo {
	
	@Id
	@Column (name = "StrategyID")
	private int strategyId;
	
	@Column (name = "Name")
	private String name;
	
	@Column (name = "Description")
	private String description;
	
	

	
	public StrategyInfo() {
		super();
	}


	public StrategyInfo(int i, String name, String description) {
		super();
		this.strategyId = i;
		this.name = name;
		this.description = description;
	}


	public int getStrategyId() {
		return strategyId;
	}


	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	
}
