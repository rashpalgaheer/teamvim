package com.Persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.Model.Strategy;

public class JPADBService {
	private EntityManagerFactory factory;
	private EntityManager em;

	public JPADBService() {
		factory = Persistence.createEntityManagerFactory("vimDB");
		em = factory.createEntityManager();
	}

	/**
	 * @return
	 */
	public int getMaxIDStrategy() {
		int ID=0;
		try {
			em.getTransaction().begin();
			ID = em.createQuery("select MAX(s.strategyId) FROM StrategyInfo s", Integer.class).getSingleResult();
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return ID;
		
	}
	
	public int getMaxIDTradingRecords(){
		int ID=0;
		try {
			em.getTransaction().begin();
			ID = em.createQuery("select MAX(s.tradeId) FROM TradingRecords s", Integer.class).getSingleResult();
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return ID;
		
	}
}
