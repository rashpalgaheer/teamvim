package com.Persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name = "TradingRecords")
public class TradingRecords {
	
	@Id
	@Column (name = "TradeID")
	private int tradeId;
	
	@Column (name = "Action")
	private String action;
	
	@Column (name = "StrategyID")
	private int strategyID;
	
	@Column (name = "TradeDate")
	private Date tradeDate;
	
	@Column (name = "StockSymbol")
	private String stockSymbol;
	
	@Column (name = "Shares")
	private int shares;
	
	@Column (name = "Price")
	private double price;
	
	
	public TradingRecords() {
		super();
	}
	
	public TradingRecords(int i, String action, int j, Date tradeDate, String stockSymbol,
			int shares, double price) {
		super();
		this.tradeId = i;
		this.action = action;
		this.strategyID = j;
		this.tradeDate = tradeDate;
		this.stockSymbol = stockSymbol;
		this.shares = shares;
		this.price = price;
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getDate() {
		return tradeDate;
	}
	public void setDate(Date date) {
		this.tradeDate = date;
	}
	public int getStrategyType() {
		return strategyID;
	}
	public void setStrategyType(int strategyType) {
		this.strategyID = strategyType;
	}
	public String getStockSymbol() {
		return stockSymbol;
	}
	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}
	public int getShares() {
		return shares;
	}
	public void setShares(int shares) {
		this.shares = shares;
	}
	
}
