package com.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;




import com.Persistence.*;
import com.Persistence.TradingRecords;

public class Trader {
	Strategy strat;
	List<Stock> listOfStocks;
	Map<String, Position> position;
	Boolean trading;
	JPADBService serv;

	public Trader() {
		strat = new TwoMA(50, 100, 2);
		listOfStocks = new ArrayList<Stock>();
		listOfStocks.add(new Stock(10.0, "AAPL", 100));
		position = new HashMap<String, Position>();
		serv = new JPADBService();
	}
	
	
	public List<Double> getStockPrices(String symbol, int periods) {
		List<Double> result = new ArrayList<Double>();
		for (int i = 0; i < periods; i++) {
			result.add(0.0);
		}
		String urlString = "http://incanada1.conygre.com:9080/prices/" + symbol + "?periods=" + periods;
 
        try {
        	URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connected :)");
            InputStream inputStream = connection.getInputStream();
            Scanner sc = new Scanner(inputStream);
            
            sc.nextLine();
            for (int i = periods-1; i >= 0; i--) {
				String s = sc.nextLine();
				String[] datas = s.split(",");

            	result.set(i, Double.parseDouble(datas[datas.length-2]));
            }
 
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        finally {
        }
		return result;
		
	}
	

	/*
	 * "<trade>\n"+
				  "<buy>true</buy>\n"+
				"<id>0</id>\n"+
				  "<price>88.0</price>\n"+
				  "<size>2000</size>\n"+
				  "<stock>HON</stock>\n"+
				 "<whenAsDate>2018-07-31T11:33:22.801-04:00</whenAsDate>\n"+
				"</trade>";
	 */

	public static void orderListener(String reply) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(reply));
			Document doc = builder.parse(is);
			NodeList nList = doc.getElementsByTagName("trade");
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void tarde() {
		int count = 0;
		while (trading) {
			for (Stock s : listOfStocks) {
				List<Double> prices = getStockPrices(s.getSymbol(), 1);
				s.setPrice(prices.get(0));
				String action = strat.getAction(s);
				if (action == "BUY") {
					String order = "<trade>\n"+
							  "<buy>true</buy>\n"+
								"<id>"+count+"</id>\n"+
								  "<price>"+s.getPrice()+"</price>\n"+
								  "<size>2000</size>\n"+	//user will tell
								  "<stock>"+s.getSymbol()+"</stock>\n"+
								 "<whenAsDate>"+new Date()+"</whenAsDate>\n"+
								"</trade>";
					// TODO: send order
					
					// TODO: receive confirmation
					TradingRecords t = new TradingRecords(1234, "BUY", 1, new Date(), s.getSymbol(), 100, 10.52);
				} else if (action == "SELL") {
					
				}
			}

		}
	}
	
	
}


class Position {
	public String symbol;
	public Long quantity;
	public String type;
}
