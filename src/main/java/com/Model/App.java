package com.Model;

/**
 * Hello world!
 *
 */
import java.io.File;
import java.util.*;

public class App 
{
    public static void main( String[] args )
    {
    	Stock s = new Stock(3.4, "AAPL",2);
        System.out.println( s.getSymbol() );
        Strategy strat = new TwoMA(50, 150, 4);

        Scanner sc = new Scanner(System.in);
        
        try {
        	sc = new Scanner(new File("test.txt"));
        } catch (Exception e) {}
        
        while (sc.hasNextLine()) {
        	Double d = sc.nextDouble();
        	String decision = strat.getAction(new Stock(d, "AAPL", 4));
        	if (decision == "BUY") {
        		System.out.println("BUY " + d.toString());
        	}
        	if (decision == "SELL") {
        		System.out.println("SELL " + d.toString());
        	}
        }
        
        
        
    }
}
