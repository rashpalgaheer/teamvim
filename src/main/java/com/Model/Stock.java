package com.Model;

public class Stock {
	private Double price;
	private String symbol;
	private int numberOfShares;
	
	public Stock() {}
	
	public Stock(Double price, String symbol, int numberOfShares) {
		this.price = price;
		this.symbol = symbol;
		this.numberOfShares = numberOfShares;
	}
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
}
