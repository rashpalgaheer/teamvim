package com.Model;

public interface Strategy {
	public String getAction(Stock stock);
}
