package com.Model;

import java.util.*;

public class TwoMA implements Strategy {
	private Map<String, Deque<Double> > map;
	private int shortTime;
	private int longTime;
	private Map<String, Double> prevShortAvg;
	private Map<String, Double> prevLongAvg;
	
	private int stratID;
	
	
	public TwoMA(int shortTime, int longTime, int stratID) {
		this.shortTime = shortTime;
		this.longTime = longTime;
		map = new HashMap<String, Deque<Double> >();
		prevShortAvg = new HashMap<String, Double>();
		prevLongAvg = new HashMap<String, Double>();
		this.stratID = stratID;
	}
	
	
	
	
	public int getID() {
		return stratID;
	}
	
	
	public Double calcAvg(String s, int time) {
		if (map.get(s).size() < time) return -1.0;
		Double sum = 0.0;
		int t = time;
		for (Double d : map.get(s)) {
			sum += d;
			t--;
			if (t <= 0) break;
		}
		return sum / time;
	}
	
	
	public String getAction(Stock stock) {
		String s = stock.getSymbol();
		if (!map.containsKey(s)) {
			map.put(s, new LinkedList<Double>());
			prevShortAvg.put(s, -1.0);
			prevLongAvg.put(s, -1.0);
			return "HOLD";
		} else {
			map.get(s).addFirst(stock.getPrice());
			Double newShortAvg = calcAvg(s, shortTime);
			Double newLongAvg = calcAvg(s, longTime);
			if ((prevShortAvg.get(s) - (-1.0) < 0.000001) ||
					(prevLongAvg.get(s) - (-1.0) < 0.000001)) {
				prevShortAvg.put(s, newShortAvg);
				prevLongAvg.put(s, newLongAvg);
				return "HOLD";
			} else if (prevShortAvg.get(s) >= prevLongAvg.get(s)) {
				prevShortAvg.put(s, newShortAvg);
				prevLongAvg.put(s, newLongAvg);
				if (newShortAvg < newLongAvg) {
					return "SELL";
				} else {
					return "HOLD";
				}
			} else { // prevShortAvg.get(s) < prevLongAvg.get(s)
				prevShortAvg.put(s, newShortAvg);
				prevLongAvg.put(s, newLongAvg);
				if (newShortAvg > newLongAvg) {
					return "BUY";
				} else {
					return "HOLD";
				}
			}
		}
		
	}
}
