/**
 * 
 */
package com.TeamVIM;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.persistence.EntityManagerFactory;

import com.Model.Trader;
import com.Persistence.JPADBService;
import com.Persistence.StrategyInfo;
import com.Persistence.TradingRecords;


/**
 * @author Administrator
 *
 */
public class DemoDriver {
	private EntityManagerFactory factory;
	private EntityManager em;

	private DemoDriver() {
		factory = Persistence.createEntityManagerFactory("vimDB");
		em = factory.createEntityManager();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DemoDriver dd = new DemoDriver();
		dd.run();
	}

	private void run() {
		
//		StrategyInfo s = new StrategyInfo("s99","twoMA", "this is used for calculating trade");
//		TradingRecords t = new TradingRecords(1234, "Buy", "s1", new Date(), "avs", 100, 10.52);
		
		
		Trader trader = new Trader();
		trader.getStockPrices("AAPL", 5);
		trader.tarde();
		
//		StrategyInfo s = new StrategyInfo(2,"twoMA", "this is used for calculating trade");
//		TradingRecords t = new TradingRecords(2, "Buy", 2, new Date(), "avs", 100, 10.52);
		System.out.println(new JPADBService().getMaxIDTradingRecords());
//		em.getTransaction().begin();
//		em.persist(s);
//		em.persist(t);
//		em.getTransaction().commit();
		
		
		//em.contains(charlotte);
		//JPAPersonService j = new JPAPersonService(factory);
		//List l =j.getAll();
		//System.out.println(em.contains(charlotte));
		
		factory.close();
		
	}

}
